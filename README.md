## Architecture project

This project is to try out and experiment with different architectural patterns and idioms. 

## Libraries currently under testing

    implementation 'androidx.core:core-ktx:1.1.0'
    implementation "androidx.fragment:fragment-ktx:1.1.0"
    implementation "androidx.activity:activity-ktx:1.0.0"

    implementation"org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlin_version"

    implementation fileTree(dir: 'libs', include: ['*.jar'])
    implementation 'androidx.appcompat:appcompat:1.1.0'
    implementation 'androidx.constraintlayout:constraintlayout:1.1.3'

    implementation "androidx.lifecycle:lifecycle-extensions:$lifecycle_version"
    implementation "androidx.lifecycle:lifecycle-viewmodel-ktx:$lifecycle_version"
    implementation "androidx.lifecycle:lifecycle-livedata-ktx:$lifecycle_version"
    implementation "androidx.lifecycle:lifecycle-runtime-ktx:$lifecycle_version"
    kapt "androidx.lifecycle:lifecycle-compiler:$lifecycle_version"

    // Android jetpack Room with kotlin + coroutines support
    implementation "androidx.room:room-runtime:$room_version"
    implementation "androidx.room:room-ktx:$room_version"
    testImplementation "androidx.room:room-testing:$room_version"
    kapt "androidx.room:room-compiler:$room_version"

    // OkHttp
    implementation 'com.squareup.okhttp3:okhttp:4.0.0'
    implementation 'com.squareup.okhttp3:logging-interceptor:4.0.0'
    testImplementation 'com.squareup.okhttp3:mockwebserver:4.0.0'

    // Retrofit
    implementation 'com.squareup.retrofit2:retrofit:2.6.0'
    implementation 'com.squareup.retrofit2:converter-gson:2.6.0'
    implementation 'com.squareup.retrofit2:adapter-rxjava2:2.6.0'

    // RxJava
    implementation 'io.reactivex.rxjava2:rxjava:2.2.10'
    implementation 'io.reactivex.rxjava2:rxandroid:2.1.1'

    // Testing mockito
    testImplementation "org.mockito:mockito-core:2.28.2"
    androidTestImplementation 'org.mockito:mockito-android:2.7.22'
    androidTestImplementation 'androidx.test:core:1.2.0'
    androidTestImplementation 'androidx.test.ext:junit:1.1.1'
    androidTestImplementation 'androidx.test:runner:1.2.0'

    // Android material library
    implementation "com.google.android.material:material:1.1.0-alpha10"

    // Coroutines
    api "org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.1"
    api "org.jetbrains.kotlinx:kotlinx-coroutines-android:1.3.1"