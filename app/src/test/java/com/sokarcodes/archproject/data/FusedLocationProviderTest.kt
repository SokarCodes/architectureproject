package com.sokarcodes.archproject.data

import android.location.Location
import com.google.android.gms.location.LocationResult
import com.sokarcodes.archproject.location.FusedLocationProvider
import com.sokarcodes.archproject.location.GoogleFusedLocationClient
import com.sokarcodes.archproject.location.UserData
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class FusedLocationProviderTest {
    @MockK
    lateinit var googleClient: GoogleFusedLocationClient

    @MockK
    lateinit var userData: UserData

    val loc = Location("MockLocation").apply {
        latitude = 65.4
        longitude = 25.3
    }

    val locationResult = LocationResult.create(listOf(loc))

    @Before
    fun setup() {
        MockKAnnotations.init(this)
    }

    @Test
    fun testLocationUpdates() {
        // Arrange
        val provider = FusedLocationProvider(
            userData,
            googleClient
        )
        coEvery { googleClient.getLastKnownLocation() } returns loc

        // Act
        val result = runBlocking { provider.getLastKnownLocation() }

        // Assert
        Assert.assertTrue(result == loc)
    }

    @Test
    fun testUserdataCalled() {
        // Arrange
        val provider = FusedLocationProvider(
            userData,
            googleClient
        )
        every { userData.setLatitude(any()) } returns Unit
        every { userData.setLongitude(any()) } returns Unit

        // Act
        provider.callback.onLocationResult(locationResult)

        // Assert
        verify {
            userData.setLatitude(loc.latitude.toFloat())
            userData.setLongitude(loc.longitude.toFloat())
        }
    }
}