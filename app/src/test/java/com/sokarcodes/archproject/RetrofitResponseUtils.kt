package com.sokarcodes.archproject

import okhttp3.Request

object RetrofitResponseUtils {
    @JvmStatic
    fun createErrorResponse(): Request {
        return Request.Builder().build()
    }
}