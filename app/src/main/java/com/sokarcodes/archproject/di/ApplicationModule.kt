/*
 * Copyright 2020 Jukka Vatjus-Anttila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sokarcodes.archproject.di

import android.content.Context
import androidx.room.Room
import com.sokarcodes.archproject.data.*
import com.sokarcodes.archproject.database.ArchDatabase
import com.sokarcodes.archproject.database.PostDao
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule {
    @Module
    public interface ApplicationModuleBinds {
        @Binds
        fun bindDatabaseAccessor(accessor: RoomDatabaseAccessor): DatabaseAccessor

        @Binds
        fun bindNetworkAccessor(accessor: RetrofitNetworkAccessor): NetworkAccessor
    }

    @Provides
    fun provideRepository(dao: DatabaseAccessor, api: NetworkAccessor): PostRepository {
        return PostRepository(dao, api)
    }

    @Singleton
    @Provides
    fun provideDatabase(application: Context): ArchDatabase {
        return Room.databaseBuilder(
            application,
            ArchDatabase::class.java,
            "ArchDatabase"
        ).build()
    }

    @Provides
    fun providePostDao(db: ArchDatabase): PostDao {
        return db.postDao()
    }
}