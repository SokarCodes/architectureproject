/*
 *
 *  * Copyright 2020 Jukka Vatjus-Anttila
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.sokarcodes.archproject.activity.main

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.juginabi.archproject.R

private const val TAG = "DetailsFragment"

class DetailsFragment : Fragment() {

    companion object {
        const val BUNDLE_POST_ID = "com.sokarcodes.archproject.activity.main.BUNDLE_POST_ID"
        const val BUNDLE_POST_TITLE = "com.sokarcodes.archproject.activity.main.BUNDLE_POST_TITLE"
        const val BUNDLE_POST_BODY = "com.sokarcodes.archproject.activity.main.BUNDLE_POST_BODY"
        const val BUNDLE_POST_USER_ID = "com.sokarcodes.archproject.activity.main.BUNDLE_POST_USER_ID"
    }

    private lateinit var idText: TextView
    private lateinit var titleText: TextView
    private lateinit var bodyText: TextView
    private lateinit var userText: TextView

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (requireActivity() as MainActivity).daggerComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.details_fragment, container, false)
        idText = rootView.findViewById(R.id.id_text)
        titleText = rootView.findViewById(R.id.title_text)
        bodyText = rootView.findViewById(R.id.body_text)
        userText = rootView.findViewById(R.id.user_text)

        idText.setText(arguments?.getLong(BUNDLE_POST_ID).toString())
        titleText.setText(arguments?.getString(BUNDLE_POST_TITLE))
        bodyText.setText(arguments?.getString(BUNDLE_POST_BODY))
        userText.setText(arguments?.getLong(BUNDLE_POST_USER_ID).toString())

        return rootView
    }
}