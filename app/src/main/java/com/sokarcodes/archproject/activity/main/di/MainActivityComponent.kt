/*
 * Copyright 2020 Jukka Vatjus-Anttila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sokarcodes.archproject.activity.main.di

import com.sokarcodes.archproject.activity.main.DetailsFragment
import com.sokarcodes.archproject.activity.main.ListFragment
import com.sokarcodes.archproject.activity.main.MainActivity
import com.sokarcodes.archproject.activity.main.ViewModelBuilderModule
import com.sokarcodes.archproject.di.ActivityScope
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [
    MainViewModelModule::class,
    ViewModelBuilderModule::class
])
interface MainActivityComponent {
    @Subcomponent.Factory
    interface Factory {
        fun create(): MainActivityComponent
    }

    fun inject(activity: MainActivity)
    fun inject(fragment: ListFragment)
    fun inject(fragment: DetailsFragment)
}