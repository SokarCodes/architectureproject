/*
 *
 *  * Copyright 2020 Jukka Vatjus-Anttila
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.sokarcodes.archproject.activity.main

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.juginabi.archproject.R
import com.sokarcodes.archproject.data.Post
import javax.inject.Inject

private const val TAG = "ListFragment"

class ListFragment : Fragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    val viewModel by viewModels<MainViewModel> { viewModelFactory }
    private val postList = mutableListOf<Post>()

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val listener = context as ItemSelectedListener

        if (listener == null) {
            throw ClassCastException("$context must implement ItemSelectedListener!")
        }

        viewAdapter = PostListAdapter(postList, listener)
        (requireActivity() as MainActivity).daggerComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.list_fragment, container, false)

        recyclerView = rootView.findViewById(R.id.myRecyclerView)
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = viewAdapter

        return rootView
    }

    override fun onResume() {
        super.onResume()
        viewModel.getAllPosts().observe(this, Observer { list ->
            postList.apply {
                clear()
                addAll(list)
            }
            viewAdapter.notifyDataSetChanged()
        })
    }
}