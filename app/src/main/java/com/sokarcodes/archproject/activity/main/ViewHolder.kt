/*
 *
 *  * Copyright 2020 Jukka Vatjus-Anttila
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.sokarcodes.archproject.activity.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.juginabi.archproject.R
import com.sokarcodes.archproject.data.Post

class PostListViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    var postId: TextView = view.findViewById(R.id.postId)
    var title: TextView = view.findViewById(R.id.title)
    var body: TextView = view.findViewById(R.id.body)
}

class PostListAdapter(private val dataset: List<Post>, val listener: ItemSelectedListener): RecyclerView.Adapter<PostListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.list_item,
            parent,
            false
        )
        return PostListViewHolder(view)
    }

    override fun getItemCount(): Int = dataset.size

    override fun onBindViewHolder(holder: PostListViewHolder, position: Int) {
        val post = dataset[position]
        holder.postId.setText(post.id.toString())
        holder.title.setText(post.title)
        holder.body.setText(post.body)

        holder.itemView.setOnClickListener { _ ->
            listener.itemSelected(post)
        }
    }
}