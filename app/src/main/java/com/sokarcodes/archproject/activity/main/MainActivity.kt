/*
 *
 *  * Copyright 2020 Jukka Vatjus-Anttila
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package com.sokarcodes.archproject.activity.main

import android.Manifest
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.commit
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.juginabi.archproject.R
import com.sokarcodes.archproject.activity.main.di.MainActivityComponent
import com.sokarcodes.archproject.core.ArchApplication
import com.sokarcodes.archproject.core.BaseActivity
import com.sokarcodes.archproject.data.Post
import com.sokarcodes.archproject.utils.PermissionUtils
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

private const val TAG = "MainActivity"

interface ItemSelectedListener {
    fun itemSelected(post: Post)
}

class MainActivity : BaseActivity(), ItemSelectedListener {
    val daggerComponent: MainActivityComponent by lazy {
        (application as ArchApplication).appComponent.mainActivityComponent().create()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        // Dagger is done. Let android framework continue from here.
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        lifecycleScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch {
                    PermissionUtils.requestPermissions(
                        this@MainActivity,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ).collect {
                        Log.d("TAG", "result! $it")
                    }
                }
            }
        }

        if (savedInstanceState == null) {
            val fragment = ListFragment()
            val transaction = supportFragmentManager.beginTransaction()
            transaction.add(R.id.fragment_holder, fragment)
            transaction.commit()
        }
    }

    override fun itemSelected(post: Post) {
        supportFragmentManager.commit {
            val fragment = DetailsFragment()
            val bundle = Bundle()
            bundle.putLong(DetailsFragment.BUNDLE_POST_ID, post.id)
            bundle.putString(DetailsFragment.BUNDLE_POST_TITLE, post.title)
            bundle.putString(DetailsFragment.BUNDLE_POST_BODY, post.body)
            bundle.putLong(DetailsFragment.BUNDLE_POST_USER_ID, post.userId)
            fragment.arguments = bundle
            addToBackStack(null)
            replace(R.id.fragment_holder, fragment)
        }
    }
}