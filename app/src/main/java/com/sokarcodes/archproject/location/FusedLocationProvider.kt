/*
 * Copyright 2020 Jukka Vatjus-Anttila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sokarcodes.archproject.location

import androidx.annotation.VisibleForTesting
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.sokarcodes.archproject.utils.ArchLog

// Stub to simulate userdata access.
class UserData {
    fun setLatitude(float: Float) {
        ArchLog.i("UserData", "Latitude updated in userdata $float")
    }

    fun setLongitude(float: Float) {
        ArchLog.i("UserData", "Longitude updated in userdata $float")
    }
}

class FusedLocationProvider(val userData: UserData, val googleClient: GoogleFusedLocationClient) {
    @VisibleForTesting
    val callback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            userData.setLatitude(locationResult.lastLocation.latitude.toFloat())
            userData.setLongitude(locationResult.lastLocation.longitude.toFloat())
        }
    }

    private val locationRequest = LocationRequest.create().apply {
        interval = 5000
        fastestInterval = 1000
        priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
    }

    fun requestLocationUpdates() {
        googleClient.start(locationRequest, callback)
    }

    fun stopLocationUdpates() = googleClient.stop(callback)

    suspend fun getLastKnownLocation() = googleClient.getLastKnownLocation()
}