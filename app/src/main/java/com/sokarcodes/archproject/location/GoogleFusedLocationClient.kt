/*
 * Copyright 2020 Jukka Vatjus-Anttila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sokarcodes.archproject.location

import android.content.Context
import android.location.Location
import android.os.Looper
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.Tasks
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GoogleFusedLocationClient(context: Context) {
    val client = LocationServices.getFusedLocationProviderClient(context)

    fun start(locationRequest: LocationRequest, callback: LocationCallback) {
        client.requestLocationUpdates(locationRequest, callback, Looper.getMainLooper())
    }

    fun stop(callback: LocationCallback) = client.removeLocationUpdates(callback)

    suspend fun getLastKnownLocation(): Location = withContext(Dispatchers.Main) {
        val location = Tasks.await(client.lastLocation)
        requireNotNull(location)
        return@withContext location
    }
}