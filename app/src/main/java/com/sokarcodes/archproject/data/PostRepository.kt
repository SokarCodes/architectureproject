/*
 * Copyright 2020 Jukka Vatjus-Anttila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sokarcodes.archproject.data

import android.util.Log
import com.sokarcodes.archproject.core.FatalNetworkException
import com.sokarcodes.archproject.utils.ArchLog
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

private const val TAG = "PostRepository"

/**
 * Repository for handling Post related access either from local database or from network resource.
 * Can be only accessed through singleton instance
 */
@Singleton
class PostRepository @Inject constructor(
    private val dao: DatabaseAccessor,
    private val api: NetworkAccessor
) {
    /**
     * Get all posts from local database.
     */
    fun getAllPosts(): Flow<List<Post>> = dao.getAllPosts()

    /**
     * Refreshes post data from network resource and on success response stores result body to
     * local database.
     */
    suspend fun refreshDataFromNetwork() {
        ArchLog.i(TAG, "Refreshing data from network.")
        try {
            val response = api.getAllPosts()
            dao.savePosts(*response.toTypedArray())
        } catch (e: FatalNetworkException) {
            Log.w(TAG, e)
        }
    }
}



