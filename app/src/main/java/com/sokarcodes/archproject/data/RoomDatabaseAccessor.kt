/*
 * Copyright 2020 Jukka Vatjus-Anttila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sokarcodes.archproject.data

import com.sokarcodes.archproject.database.PostDao
import com.sokarcodes.archproject.database.PostEntity
import com.sokarcodes.archproject.utils.ArchLog
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext
import javax.inject.Inject

private const val TAG = "RoomDatabaseAccessor"

/**
 * Class for accessing [Post] table in Room database
 *
 * @param dao Room postDao object is passed in as parameter. All DB requests are converted to
 * intermediate format. @see [Post] used here.
 */
class RoomDatabaseAccessor @Inject constructor(private val dao: PostDao) : DatabaseAccessor {
    override fun getAllPosts(): Flow<List<Post>> {
        return dao.getAllPosts()
            .map { postEntityList ->
                postEntityList.map { postEntity ->
                    Post(postEntity.id, postEntity.title, postEntity.body, postEntity.userId)
                }
            }
    }

    override fun getPost(id: Long): Flow<Post> {
        return dao.getPost(id)
            .distinctUntilChanged()
            .map {
                Post(it.userId, it.title, it.body, it.userId)
            }
    }

    override suspend fun savePosts(vararg posts: Post) = withContext(Dispatchers.IO) {
        ArchLog.i(TAG, "Saving posts!")
        val arrayOfPosts = posts.map {
            PostEntity(
                it.id,
                it.title,
                it.body,
                it.userId
            )
        }.toTypedArray()

        dao.insertAll(*arrayOfPosts)
    }
}