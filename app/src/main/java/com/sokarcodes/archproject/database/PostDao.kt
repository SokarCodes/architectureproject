/*
 * Copyright 2020 Jukka Vatjus-Anttila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sokarcodes.archproject.database

import androidx.room.*
import kotlinx.coroutines.flow.Flow

/**
 * Room DAO interface which is used to generate room DAO implementation code
 */
@Dao
interface PostDao {
    @Query("SELECT * FROM posts")
    fun getAllPosts(): Flow<List<PostEntity>>

    @Query("SELECT * FROM posts WHERE id IN (:postId)")
    fun getPost(postId: Long): Flow<PostEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg posts: PostEntity)

    @Delete
    fun deletePost(post: PostEntity)
}