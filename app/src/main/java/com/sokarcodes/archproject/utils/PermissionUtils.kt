/*
 * Copyright 2020 Jukka Vatjus-Anttila
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sokarcodes.archproject.utils

import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch

/**
 * Android permission related utilities
 */
class PermissionUtils {

    companion object {
        /**
         * Reactively request all the permissions listed as second parameter
         * @param context Fragment activity where this query originates
         * @param permissions variable argument list of string
         */
        @JvmStatic
        fun requestPermissions(context: FragmentActivity,
                               vararg permissions: String): Flow<PermissionResponse> {
            // Check if permissions already granted
            var allGranted = true

            val map = mutableMapOf<String, Boolean>()

            for (perm in permissions) {
                val currentGranted =
                    ContextCompat.checkSelfPermission(context, perm) == PERMISSION_GRANTED
                allGranted = allGranted and currentGranted
                map[perm] = currentGranted
            }

            if (allGranted) {
                return flow { emit(PermissionResponse(true, map)) }
            }

            val instance =
                PermissionQueryFragment.newInstance(
                    arrayOf(*permissions)
                )
            context.supportFragmentManager
                .beginTransaction()
                .add(instance,
                    PermissionQueryFragment.TAG
                )
                .commitAllowingStateLoss()

            return instance.getPermissionFlow()
        }
    }

    /**
     * Fragment used in permission requests.
     */
    class PermissionQueryFragment : Fragment() {
        private val permissionFlow = MutableSharedFlow<PermissionResponse>()

        companion object {
            const val TAG = "PermissionQueryFragment"
            const val PERMISSIONS_EXTRA = "com.sokarcodes.archproject.PERMISSION_EXTRA"
            const val PERMISSIONS_REQUEST = 1000

            @JvmStatic
            fun newInstance(permissions: Array<String>): PermissionQueryFragment {
                return PermissionQueryFragment()
                    .apply {
                    val extras = Bundle().apply { putStringArray(PERMISSIONS_EXTRA, permissions) }
                    arguments = extras
                }
            }
        }

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            val permissions = arguments?.getStringArray(PERMISSIONS_EXTRA)
            if (permissions != null && permissions.isNotEmpty()) {
                requestPermissions(permissions,
                    PERMISSIONS_REQUEST
                )
            } else {
                pop()
            }
        }

        override fun onRequestPermissionsResult(
            requestCode: Int, permissions: Array<out String>,
            grantResults: IntArray) {
            if (requestCode == PERMISSIONS_REQUEST) {
                // Permission request completed
                val map = mutableMapOf<String, Boolean>()
                var granted = true
                for (i in permissions.indices) {
                    granted = granted and (grantResults[i] == PERMISSION_GRANTED)
                    map[permissions[i]] = grantResults[i] == PERMISSION_GRANTED
                }
                lifecycleScope.launch {
                    permissionFlow.emit(PermissionResponse(granted, map))
                }
            } else {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            }
        }

        fun getPermissionFlow(): Flow<PermissionResponse> {
            return permissionFlow.asSharedFlow()
        }

        private fun pop() {
            requireActivity().supportFragmentManager
                .beginTransaction()
                .remove(this)
                .commitAllowingStateLoss()
        }
    }
}

/**
 * Data class for storing granular information of permission queries.
 *
 * @param granted Boolean stating if all permissions are granted.
 * @param status Map of all permission grant status
 */
data class PermissionResponse(val granted: Boolean, val status: Map<String, Boolean>) {
    override fun toString(): String {
        var result = ""
        for ((key, value) in status) {
            result = result + "$key: " + if (value) "Granted\n" else "Denied\n"
        }
        return result
    }
}